# Clojure starter project

Basic demo Clojure project.

- runs in Docker
- clj-kondo linter
- basic tests and logging
- GitLab CI integration


First time, and if you change dependencies or Dockerfile:

    `docker-compose build`

To test:

    `docker-compose run starter lein test`

To run:

    `docker-compose run starter lein run`

