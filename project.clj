(defproject starter-project "0.1.0"
  :description "Clojure starter project"
  :url "https://gitlab.com/crossref/clojure_starter_project"
  :main crossref.starter
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [environ "1.1.0"]
                 [com.taoensso/timbre "3.4.0"]])
