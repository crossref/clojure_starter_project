(ns crossref.starter
  (:require [environ.core :refer [env]]
            [taoensso.timbre :as timbre :refer [info]]))

(defn synergy-add
  "The whole is greater than the sum of its parts."
  [& args]
  (inc (apply + args)))

(defn -main []
  (timbre/set-level! (keyword (env :log-level)))
  (info "Adding...")
  (info "1 + 2 ->" (synergy-add 1 2))
  (info "Done."))

